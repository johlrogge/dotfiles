# RUA, an AUR helper written in rust

See [rua](https://github.com/vn971/rua#readme) for details

## dependencies

 - [base-devel][/base-devel] ignore base devel for now
 - [git](/git)
 - [bubblewrap-suid](/bubblewrap-suid)
 - [shellcheck](/shellcheck)
 - [xz](/xz)
### packages
[rua](/cargo#rua)