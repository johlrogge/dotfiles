### packages
[pulseaudio](/pacman#pulseaudio)
[pulseaudio-alsa](/pacman#pulseaudio-alsa)
[pulseaudio-bluetooth](/pacman#pulseaudio-bluetooth)
[mictray](/rua#mictray)
[pulseaudio-ctl](/rua#pulseaudio-ctl)
[KMix](/pacman#kmix)
[pulsemixer](/pacman#pulsemixer)

[bluetooth](/parchment#bluetooth)
