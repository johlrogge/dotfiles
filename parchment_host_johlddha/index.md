# Work laptop

## ricing
 - [nerdfonts](/nerdfonts)

## command line tools
 - [alacritty - a grapic accellerated simple terminal](/alacritty)
 - [zsh - the shell of choice](/zsh)
 - [unzip](/unzip)

## security
 - [security, setup ssh and other stuff](/security)

## graphical environment
### display manager
### window manager
 - [anarchywm](/anarchywm)

### general
- [xsel](/pacman#xsel): A clipboard integration utility
- [xdg-utils](/xdg-utils)

# workflows
 - [programming](/workflow-programming)

## web

### site generators
 - [hugo](/hugo)

### browsers
 - [brave](/brave)

