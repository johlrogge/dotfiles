# LightDm

A light weight display manager

## setting a greeter


in _/etc/lightdm.conf_:
```
[seat:*]
...
greeter-session=lightdm-yourgreeter-greeter
...
```

## enable lightdm
```
$ systemctl enable lightdm
```

### packages
[lightdm](/pacman#lightdm)
[lightdm-gtk-greeter](/pacman#lightdm-gtk-greeter)
[light-locker](/pacman#light-locker)
