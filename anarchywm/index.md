# Anarchy WM

## dependencies
 - [Program launcher - rofi](/rofi)
 - [Image background - feh](/feh)
 - [bar - polybar](/polybar)

### packages
[anarchywm/anarchywm](/projects-gitlab#anarchywm/anarchywm)
