export PATH="$HOME/.cargo/bin:$HOME/parchment:$PATH:$HOME/.local/bin/"
export LC_MESSAGES="en_US.UTF-8"
export EDITOR="helix"
export JAVA_HOME="/usr/lib/jvm/java-15-amazon-corretto/"
eval "$(starship init zsh)"

# The following lines were added by compinstall
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _list _ignored _match _approximate _prefix
zstyle ':completion:*' completions 1
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' glob 1
zstyle ':completion:*' group-name ''
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' match-original both
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}' 'r:|[._-]=* r:|=*' '' 'l:|=* r:|=*'
zstyle ':completion:*' max-errors 2
zstyle ':completion:*' menu select=2
zstyle ':completion:*' original true
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' prompt 'errors: %e >'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' substitute 1
zstyle ':completion:*' use-compctl true
zstyle ':completion:*' word true
zstyle :compinstall filename '/home/johlrogge/.config/zsh/.zshrc'

autoload -Uz compinit

compinit -d ~/.cache/zsh/zcompdump-$ZSH_VERSION
# End of lines added by compinstall
# Lines configured by zsh-newuser-install

HISTFILE=~/.cache/zsh/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob notify
setopt share_history append_history inc_append_history \
       hist_expire_dups_first hist_find_no_dups hist_reduce_blanks \
       hist_verify

bindkey -v
bindkey '^R' history-incremental-pattern-search-backward
# End of lines configured by zsh-newuser-install

source ${ZDOTDIR}.aliases

# setup autoloads
ZAUTOLOADS=${ZDOTDIR}autoloads/
fpath+=$ZAUTOLOADS
for f in $ZAUTOLOADS/*; do
   autoload ${f##*/}
done

# setup completions
ZCOMPLETIONS=${ZDOTDIR}completions/
fpath+=$ZCOMPLETIONS

# configure other programs
ZCONFIGURATION=${ZDOTDIR}configuration/
for f in $ZCONFIGURATION/*; do
    source $f
done

eval `dircolors`
BAT_THEME='zenburn'

# colorful manpages
man() {
    env \
        LESS_TERMCAP_mb=$(printf "\e[1;31m") \
        LESS_TERMCAP_md=$(printf "\e[1;31m") \
        LESS_TERMCAP_me=$(printf "\e[0m") \
        LESS_TERMCAP_se=$(printf "\e[0m") \
        LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
        LESS_TERMCAP_ue=$(printf "\e[0m") \
        LESS_TERMCAP_us=$(printf "\e[1;32m") \
            man "$@"
}
