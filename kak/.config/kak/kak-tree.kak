
plug "ul/kak-tree" do %{
    cargo install --path . --force --features "rust javascript"
} config %{
    hook global WinSetOption filetype=(rust) %{
        declare-user-mode syntax-tree-children
        map global syntax-tree-children c ': tree-select-children<ret>' -docstring 'children'
        map global syntax-tree-children f ': tree-select-children fn_item<ret>' -docstring 'fn_item'
        map global syntax-tree-children l ': tree-select-children func_literal<ret>' -docstring 'func_literal'
        map global syntax-tree-children s ': tree-select-children rust_statement<ret>' -docstring 'rust_statement'
        map global syntax-tree-children b ': tree-select-children block<ret>' -docstring 'block'
        map global syntax-tree-children i ': tree-select-children impl_item<ret>' -docstring 'impl_item'
        map global syntax-tree-children <backspace> ': enter-user-mode syntax-tree<ret>' -docstring 'back...'

        declare-user-mode syntax-tree-parent
        map global syntax-tree-parent p ': tree-select-parent-node<ret>' -docstring 'parent_node'
        map global syntax-tree-parent f ': tree-select-parent-node fn_item<ret>' -docstring 'fn_item'
        map global syntax-tree-parent b ': tree-select-parent-node block<ret>' -docstring 'block'
        map global syntax-tree-parent i ': tree-select-parent-node impl_item<ret>' -docstring 'impl_item'
        map global syntax-tree-parent <backspace> ': enter-user-mode syntax-tree<ret>' -docstring 'back...'

        declare-user-mode syntax-tree-next
        map global syntax-tree-next ')' ': tree-select-next-node<ret>' -docstring 'next_node'
        map global syntax-tree-next f   ': tree-select-next-node fn_item<ret>' -docstring 'fn_item'
        map global syntax-tree-next b   ': tree-select-next-node block<ret>' -docstring 'block'
        map global syntax-tree-next i   ': tree-select-next-node impl_item<ret>' -docstring 'impl_item'
        map global syntax-tree-next <backspace> ': enter-user-mode syntax-tree<ret>' -docstring 'back...'

        declare-user-mode syntax-tree-prev
        map global syntax-tree-prev '(' ': tree-select-previous-node<ret>' -docstring 'previous_node'
        map global syntax-tree-prev f ': tree-select-previous-node fn_item<ret>' -docstring 'fn_item'
        map global syntax-tree-prev b ': tree-select-previous-node block<ret>' -docstring 'block'
        map global syntax-tree-prev i ': tree-select-previous-node impl_item<ret>' -docstring 'impl_item'
        map global syntax-tree-prev <backspace> ': enter-user-mode syntax-tree<ret>' -docstring 'back...'

        declare-user-mode syntax-tree
        map global syntax-tree '(' ': enter-user-mode syntax-tree-prev<ret>' -docstring 'previous_node'
        map global syntax-tree ')' ': enter-user-mode syntax-tree-next<ret>' -docstring 'next_node'
        map global syntax-tree c ': enter-user-mode syntax-tree-children<ret>' -docstring 'children'
        map global syntax-tree p ': enter-user-mode syntax-tree-parent<ret>' -docstring 'parent_node'
        map global syntax-tree t ': tree-node-sexp<ret>' -docstring 'tree-node-sexp'
        map global user s ': enter-user-mode syntax-tree<ret>' -docstring 'tree select'
    }
}
