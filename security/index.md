# Security

Things needed for secure communication

## SSH ergonomics

[openssh](/openssh) good old heart bleed
[keychain](/keychain) is program designed to help you easily manage your SSH keys with minimal interaction. Maintains a single _ssh-agent_ process across multiple login sessions.
[x11-ssh-askpass](/x11-ssh-askpass) provides a graphical dialog for entering your passphrase when running an X sesssion.
