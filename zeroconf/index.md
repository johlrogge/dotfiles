## Zeroconf
discover services on the local network

### packages

systemd-resolved has a built-in mDNS service, make sure to disable systemd-resolved's multicast DNS resolver/responder (refer to resolved.conf(5)) or disable systemd-resolved.service entirely before using Avahi.
[avahi](/pacman#avahi)
