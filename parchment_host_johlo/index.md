# Work from home computer

[Librem mini v2](/computer_librem_mini_v2)

## ricing
 - [nerdfonts](/nerdfonts)

## command line tools
 - [alacritty - a grapic accellerated simple terminal](/alacritty)
 - [zsh - the shell of choice](/zsh)
 - [unzip](/unzip)
 - [nnn](/nnn): a full-featured terminal file manager

## security
 - [security, setup ssh and other stuff](/security)

## graphical environment
 - [xorg](/xorg-server)

### display manager
 - [LightDm](/lightdm)

### window manager
 - [anarchywm](/anarchywm)

### general
- [xsel](/pacman#xsel): A clipboard integration utility
- [xdg-utils](/xdg-utils)

# workflows
 - [programming](/workflow-programming)

## web

### site generators
 - [hugo](/hugo)
 - [zola](/zola)

### browsers
 - [brave](/brave)

### graphics
 - [inkscape](/inkscape)