# programming

Tools needed primarilly for programming

## editors
 - [vscode](/vscode)
 - [helix](/helix)

## vs
 - [git](/git)

## command line tools
 - [jq](/pacman#jq): A tool to query, format and transform json

## reading
 - [markdown](/markdown)

## virtualization
 - [virtual box](/pacman#virtualbox)

## languages

### yaml
 - [yamllint](/pacman#yamllint)

### rust
 - [rust](/rust)

### clojure
 - [clojure](/clojure)
 - [babashka](/babashka)

## debugging
 - [gdb](/pacman#gdb)
 - [lldb](/pacman#lldb)

## docker
 - [docker](/docker)

## projects
 - [Mikado Tool](/project-agical-mikado)
 - [Cospaia](/project-cospaia) 
 - [Crates](/crates)
 - [House-bandit](/projects-gitlab#house-bandit/house-bandit)
 - [rss-tools](/project-rss-tools)
 
## contributions
 - [Calva dram](/contrib-calva-dram)

